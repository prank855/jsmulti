var Component = require("./Component");
class Player_Script extends Component {
  constructor(parent) {
    super(parent);
    this.name = "Player_Script";
    this.speed = 100;
  }
  init() {
    this.transform = this.parent.getComponent("Transform");
  }

  update(deltaTime) {}

  networkInput(input, deltaTime) {
    if (input.includes("w")) {
      this.transform.position.y -= this.speed * deltaTime;
    }
    if (input.includes("a")) {
      this.transform.position.x -= this.speed * deltaTime;
    }
    if (input.includes("s")) {
      this.transform.position.y += this.speed * deltaTime;
    }
    if (input.includes("d")) {
      this.transform.position.x += this.speed * deltaTime;
    }
  }
}
module.exports = Player_Script;
