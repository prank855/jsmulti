var Component = require("./Component");
var Vector2 = require("./Vector2");
class Transform extends Component {
  constructor(parent) {
    super(parent);
    this.name = "Transform";
    this.position = new Vector2();
    this.networkParams = ["position"];
    this.networkLerp = ["position"];
  }
}
module.exports = Transform;
