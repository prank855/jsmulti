var Component = require("./Component");
class SpriteRenderer extends Component {
  constructor(parent) {
    super(parent);
    this.name = "SpriteRenderer";
    this.networkParams = ["img"];
  }

  setSprite(spriteName) {
    this.img = spriteName;
  }
}
module.exports = SpriteRenderer;
