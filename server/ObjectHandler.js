var GameObject = require("./GameObject");
class ObjectHandler {
  constructor() {
    this.gameObjects = [];
    this.lastGameID = 0;
    this.networkInputQueue = [];
  }

  addGameObject(go) {
    for (var _go of this.gameObjects) {
      if (_go.name == go.name) {
        //console.log(`(${_go.name}) already exists within Object Handler, renaming to: (${go.name}(1))`);
      }
    }
    go.id = this.lastGameID++;
    this.gameObjects.push(go);
    console.log(`Added [${go.name}] to Object Handler.`);
  }

  findGameObject(goName) {
    //TODO: find Game Object(s) by its name
  }

  getGameObject(goID) {
    for (var go of this.gameObjects) {
      if (goID == go.id) {
        return go;
      }
    }
    throw `Could not find a Game Object with ID: ${goID}`;
  }

  start() {
    this.createGame();
    for (var go of this.gameObjects) {
      go.start();
    }
  }

  update(deltaTime) {
    for (var go of this.gameObjects) {
      go.update(deltaTime);
    }
  }

  createGame() {}

  networkInput(deltaTime) {
    for (var a of this.networkInputQueue) {
      var socketID = a.id;
      var input = a.data;
      for (var go of this.gameObjects) {
        if (go.networkOwnerID == socketID) {
          go.networkInput(input, deltaTime);
        }
      }
    }
    this.networkInputQueue = [];
  }

  getGameState() {
    var temp = [];
    for (var go of this.gameObjects) {
      temp.push(go.toNetworkObject());
    }
    return temp;
  }
}
module.exports = ObjectHandler;
