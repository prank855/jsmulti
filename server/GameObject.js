class GameObject {
  constructor(name) {
    this.name = name;
    this.id = null;
    this.networkOwnerID = "";
    this.components = [];
    console.log(`[${name}] Initialized as a Game Object.`);
  }

  addComponent(name) {
    for (var co of this.components) {
      if (co.name == name) {
        throw `(${name}) already exists within [${this.name}].`;
      }
    }
    var co = require("./" + name);
    var temp = new co(this);
    temp.init();
    this.components.push(temp);
    console.log(`Added (${name}) to [${this.name}].`);
  }

  getComponent(name) {
    for (var co of this.components) {
      if (co.name == name) {
        return co;
      }
    }
    throw `(${name}) does not exist in [${this.name}].`;
  }

  start() {
    for (var co of this.components) {
      co.start();
    }
  }

  update(deltaTime) {
    for (var co of this.components) {
      co.update(deltaTime);
    }
  }

  networkInput(input, deltaTime) {
    for (var co of this.components) {
      co.networkInput(input, deltaTime);
    }
  }

  toNetworkObject() {
    var temp = {};
    temp.name = this.name;
    temp.id = this.id;
    temp.networkOwnerID = this.networkOwnerID;
    temp.components = [];
    for (var co of this.components) {
      temp.components.push(co.toNetworkObject());
    }
    return temp;
  }
}
module.exports = GameObject;
