var ObjectHandler = require("./ObjectHandler");
var GameObject = require("./GameObject");
var Time = require("./Time");
var fs = require("fs");
class Game {
  constructor() {
    this.io = null;
    this.tick = 0;
    this.tickrate = 60;
    this.handler = new ObjectHandler();
    this.time = new Time();
    this.count = 0;
  }

  socketConnectionEvent(socket) {
    this.registerSocketEvents(socket);

    var go = new GameObject(`Network Player: ${socket.id}`);
    go.networkOwnerID = socket.id;
    go.addComponent("Transform");
    var pos = go.getComponent("Transform").position;
    pos.x = Math.random() * 1280;
    pos.y = Math.random() * 720;
    go.addComponent("Player_Script");
    go.addComponent("SpriteRenderer");
    go.getComponent("SpriteRenderer").setSprite("Player");
    go.networkOwnerID = socket.id;
    this.handler.addGameObject(go);
  }

  registerSocketEvents(socket) {
    var self = this;
    //test
    socket.on("Client Input", function(data) {
      //console.log(`Client ID: ${socket.id}, Data: ${data}`);
      self.handler.networkInputQueue.push({ id: socket.id, data: data });
    });
    socket.on("Ping", function(data) {
      socket.emit("Pong", data);
    });
    socket.on("disconnect", function(data) {
      for (var go of self.handler.gameObjects) {
        if (go.networkOwnerID == socket.id) {
          self.handler.gameObjects.splice(
            self.handler.gameObjects.indexOf(go),
            1
          );
          return;
        }
      }
    });
  }

  start(socketio) {
    this.io = socketio;
    console.log("Game Server Started.");
    this.handler.start();
    this.loop();
  }

  loop() {
    var self = this;
    this.time.update();
    this.tick++;
    this.handler.update(this.time.deltaTime);
    this.handler.networkInput(this.time.deltaTime);

    this.io.emit("tick", {
      tick: self.tick,
      gameState: this.handler.getGameState()
    });

    setTimeout(function() {
      self.loop();
    }, 1000 / this.tickrate);
  }
}
module.exports = Game;
