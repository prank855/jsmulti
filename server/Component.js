class Component {
  constructor(parent) {
    this.parent = parent;
    this.name = "Unnamed Component";
    this.networkParams = [];
    this.networkLerp = [];
  }

  init() {}
  start() {}
  update(deltaTime) {}
  networkInput(input, deltaTime) {}
  toNetworkObject() {
    var temp = {};
    temp.name = this.name;
    temp.params = [];
    for (var param of this.networkParams) {
      var $temp = {};
      $temp.data = this[param];
      $temp.name = param;
      temp.params.push($temp);
    }
    temp.lerp = [];
    for (var l of this.networkLerp) {
      temp.lerp.push(l);
    }
    return temp;
  }
}
module.exports = Component;
