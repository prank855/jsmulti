class GameObject {
  constructor(name) {
    this.name = name;
    this.components = [];
    this.networkOwnerID = "";
    this.justCreated = true;
    //console.log(`[${name}] Initialized as a Game Object.`);
  }

  addComponent(name) {
    for (var co of this.components) {
      if (co.name == name) {
        throw `(${name}) already exists within [${this.name}].`;
      }
    }
    var temp = eval(`new ${name}(this)`);
    temp.init();
    this.components.push(temp);
    //console.log(`Added (${name}) to [${this.name}].`);
  }

  getComponent(name) {
    for (var co of this.components) {
      if (co.name == name) {
        return co;
      }
    }
    throw `(${name}) does not exist in [${this.name}].`;
  }

  start() {
    for (var co of this.components) {
      co.start();
    }
  }

  update(time) {
    for (var co of this.components) {
      co.update(time.deltaTime);
      if (!co.bypassLerp && !co.lerpData.length == 0) {
        co.lerp(time.deltaTime, time.ping);
      }
    }
  }

  render(ctx) {
    for (var co of this.components) {
      co.render(ctx);
    }
  }

  networkInput(input, deltaTime, ping) {
    for (var co of this.components) {
      co.networkInput(input, deltaTime, ping);
    }
  }
}
