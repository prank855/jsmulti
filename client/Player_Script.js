class Player_Script extends Component {
  constructor(parent) {
    super(parent);
    this.name = "Player_Script";
    this.speed = 100;
    this.lastInputTime = Date.now();
  }
  init() {
    this.transform = this.parent.getComponent("Transform");
  }

  update(deltaTime) {}

  networkInput(input, deltaTime, ping) {
    var movCheck = false;
    if (input.includes("w")) {
      this.transform.position.y -= this.speed * deltaTime;
      movCheck = true;
    }
    if (input.includes("a")) {
      this.transform.position.x -= this.speed * deltaTime;
      movCheck = true;
    }
    if (input.includes("s")) {
      this.transform.position.y += this.speed * deltaTime;
      movCheck = true;
    }
    if (input.includes("d")) {
      this.transform.position.x += this.speed * deltaTime;
      movCheck = true;
    }
    //console.log(`last: ${this.lastInputTime} now: ${Date.now()} ping: ${ping}`);
    if (movCheck) {
      this.lastInputTime = Date.now();
      this.transform.bypassLerp = true;
    } else if (this.lastInputTime + ping * 4 < Date.now()) {
      this.transform.bypassLerp = false;
    }
  }
}
