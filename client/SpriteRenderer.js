class SpriteRenderer extends Component {
  constructor(parent) {
    super(parent);
    this.name = "SpriteRenderer";
  }

  init() {
    this.prerender = document.createElement("canvas");
    this.prerender.width = 0;
    this.transform = this.parent.getComponent("Transform");
  }

  setSprite(spriteName) {
    this.img = spriteManager.getSprite(spriteName);
  }

  render(ctx) {
    if (!this.img) {
      throw "No Sprite Found";
    }
    ctx.drawImage(
      spriteManager.getSprite(this.img),
      this.transform.position.x,
      this.transform.position.y
    );
    /*
    ctx.drawImage(
      this.prerender,
      0,
      0,
      32,
      32,
      this.transform.position.x -
        (handler.camera.position.x - (canvas.width / 2) * handler.camera.zoom) *
          handler.camera.zoom,
      parent.position.y -
        (handler.camera.position.y -
          (canvas.height / 2) * handler.camera.zoom) *
          handler.camera.zoom,
      this.img.width * handler.camera.zoom,
      this.img.height * handler.camera.zoom
    );
    */
  }
}
