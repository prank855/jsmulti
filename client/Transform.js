class Transform extends Component {
  constructor(parent) {
    super(parent);
    this.name = "Transform";
    this.position = new Vector2();
  }
  lerp(deltaTime, ping) {
    var xChange = (this.lerpData[0].data.x - this.position.x) * deltaTime * 10;
    var yChange = (this.lerpData[0].data.y - this.position.y) * deltaTime * 10;
    var xCheck = false;
    var yCheck = false;
    if (xChange > 0 && this.position.x + xChange > this.lerpData[0].data.x) {
      this.position.x = this.lerpData[0].data.x;
      xCheck = true;
    }
    if (xChange < 0 && this.position.x + xChange < this.lerpData[0].data.x) {
      this.position.x = this.lerpData[0].data.x;
      xCheck = true;
    }
    if (yChange > 0 && this.position.y + yChange > this.lerpData[0].data.y) {
      this.position.y = this.lerpData[0].data.y;
      yCheck = true;
    }
    if (yChange < 0 && this.position.y + yChange < this.lerpData[0].data.y) {
      this.position.y = this.lerpData[0].data.y;
      yCheck = true;
    }
    if (!xCheck) {
      this.position.x += xChange;
    }
    if (!yCheck) {
      this.position.y += yChange;
    }
  }
}
