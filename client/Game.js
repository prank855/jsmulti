var spriteManager = new SpriteManager();
class Game {
  constructor() {
    this.time = new Time();
    this.handler = new ObjectHandler();
    this.serverTick = null;
    this.framerate = 200;
    this.socket = null;
    this.inputArray = [];
    this.inputArrayRemoveQueue = [];
    this.started = false;
    this.pingCount = 0;
    this.pingQueue = [];
  }

  createCanvas() {
    this.canvas = document.createElement("canvas");
    this.canvas.id = "canvas";
    this.canvas.width = 1280;
    this.canvas.height = 720;
    this.canvas.style =
      "position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;";
    this.ctx = this.canvas.getContext("2d");
    this.ctx["imageSmoothingEnabled"] = false; /* standard */
    this.ctx["mozImageSmoothingEnabled"] = false; /* Firefox */
    this.ctx["oImageSmoothingEnabled"] = false; /* Opera */
    this.ctx["webkitImageSmoothingEnabled"] = false; /* Safari */
    this.ctx["msImageSmoothingEnabled"] = false; /* IE */
    document.getElementById("g").appendChild(this.canvas);
    document.body.style.backgroundColor = "rgba(40, 40, 40, 1)";
  }

  start() {
    this.handler.gameObjects = [];
    if (this.started) {
      return;
    }
    this.started = true;
    console.log("Game Client Started");
    this.createCanvas();
    this.registerInputEvents();

    spriteManager.addSprite("Player", "img/Player.png");
    /*
    var tempGO = new GameObject("Player");
    tempGO.networkOwnerID = this.socket.id;
    console.log(this.socket.id);
    tempGO.addComponent("Transform");
    tempGO.addComponent("Player_Script");
    tempGO.addComponent("SpriteRenderer");
    tempGO.getComponent("SpriteRenderer").setSprite("Player");
    this.handler.addGameObject(tempGO);
    */

    this.handler.start();
    this.loop();
  }

  registerInputEvents() {
    var self = this;
    document.addEventListener("keydown", function(data) {
      var check = false;
      for (var key of self.inputArray) {
        if (key == data.key) {
          check = true;
        }
      }
      if (!check) {
        self.inputArray.push(data.key);
      }
    });
    document.addEventListener("keyup", function(data) {
      self.inputArrayRemoveQueue.push(data.key);
    });
  }

  registerSocket(socket) {
    this.socket = socket;
    this.registerSocketEvents();
  }

  registerSocketEvents() {
    var self = this;
    this.socket.on("connect", function(data) {
      console.log("Connected");
      self.start();
    });
    this.socket.on("tick", function(data) {
      self.serverTickEvent(data);
    });
    this.socket.on("Pong", function(data) {
      for (var p of self.pingQueue) {
        if (p.count == data) {
          self.time.ping = Date.now() - p.time;
          self.pingQueue.splice(self.pingQueue.indexOf(p), 1);
        }
      }
    });
  }

  serverTickEvent(data) {
    this.serverTick = data.tick;
    this.handler.serverUpdateEvent(data.gameState);
    for (var key of this.inputArrayRemoveQueue) {
      this.inputArray.splice(this.inputArray.indexOf(key), 1);
    }
    this.socket.emit("Client Input", this.inputArray);
    this.socket.emit("Ping", this.pingCount++);
    this.pingQueue.push({ time: Date.now(), count: this.pingCount });
    this.inputArrayRemoveQueue = [];
  }

  loop() {
    var self = this;
    this.time.update();
    this.handler.update(this.time);
    this.handler.networkInput(
      this.socket.id,
      this.inputArray,
      this.time.deltaTime,
      this.time.ping
    );

    this.handler.render(this.ctx);
    setTimeout(function() {
      self.loop();
    }, 1000 / this.framerate);
  }
}
