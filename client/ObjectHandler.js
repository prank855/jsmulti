class ObjectHandler {
  constructor() {
    this.gameObjects = [];
  }

  addGameObject(_go) {
    for (var go of this.gameObjects) {
      if (go.name == _go.name) {
      }
    }
    this.gameObjects.push(_go);
    //console.log(`Added [${_go.name}] to Object Handler.`);
  }

  getGameObject(goID) {
    for (var go of this.gameObjects) {
      if (goID == go.id) {
        return go;
      }
    }
    return false;
    throw `Could not find a Game Object with ID: ${goID}`;
  }

  start() {
    for (var go of this.gameObjects) {
      go.start();
    }
  }

  update(time) {
    for (var go of this.gameObjects) {
      go.update(time);
    }
  }

  render(ctx) {
    ctx.fillStyle = "Blue";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    for (var go of this.gameObjects) {
      go.render(ctx);
    }
  }

  serverUpdateEvent(data) {
    for (var go of data) {
      var selfGo = this.getGameObject(go.id);
      if (!selfGo) {
        var tempGO = new GameObject(go.name);
        tempGO.id = go.id;
        tempGO.networkOwnerID = go.networkOwnerID;
        for (var co of go.components) {
          tempGO.addComponent(co.name);
        }
        this.addGameObject(tempGO);
      }
      selfGo = this.getGameObject(go.id);
      for (var co of go.components) {
        for (var selfCO of selfGo.components) {
          if ((selfCO.name = co.name)) {
            for (var param of co.params) {
              var lerpCheck = false;
              if (co.lerp.includes(param.name) && !selfGo.justCreated) {
                selfCO.lerpData = [];
                selfCO.lerpData.push(param);
              } else {
                selfCO[param.name] = param.data;
                selfGo.justCreated = false;
              }
            }
          }
        }
      }
    }
  }

  networkInput(socketID, input, deltaTime, ping) {
    for (var go of this.gameObjects) {
      if (go.networkOwnerID == socketID) {
        go.networkInput(input, deltaTime, ping);
      }
    }
  }
}
