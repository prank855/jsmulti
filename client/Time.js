class Time {
  constructor(performance) {
    this.lastTime = Date.now();
    this.deltaTime = 0;
    this.ping = 1;
  }
  update() {
    var t = Date.now();
    this.deltaTime = (t - this.lastTime) / 1000;
    this.lastTime = t;
  }
}
