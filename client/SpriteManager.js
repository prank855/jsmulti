class SpriteManager {
  constructor() {
    this.dictionary = new Map();
    this.sprites = 0;
    this.loaded = 0;
  }

  getSprite(name) {
    if (name == null) {
      return null;
    }
    return this.dictionary.get(name);
  }

  addSprite(name, location) {
    this.sprites++;
    var temp = new Image();
    temp.onload = function() {
      spriteManager.loaded++;
      if (spriteManager.loaded == spriteManager.sprites) {
        console.log("Loaded all", spriteManager.sprites, "sprites");
        //spriteManager.finish();
      }
    };
    temp.src = location;
    this.dictionary.set(name, temp);
  }
}
