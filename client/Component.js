class Component {
  constructor(parent) {
    this.parent = parent;
    this.name = "Unnamed Component";
    // {param: name, data: data}
    this.lerpData = [];
    this.bypassLerp = false;
  }

  init() {}
  start() {}
  update(deltaTime) {}
  render() {}
  networkInput(input, deltaTime, ping) {}
  lerp(deltaTime, ping) {}
}
