/// express server
var express = require("express");
var app = express();
var http = require("http").createServer(app);
const port = 1337;

app.use(express.static(__dirname + "/client"));

http.listen(port, function() {
  console.log(`listening on *:${port}`);
});

/// Game Setup
var io = require("socket.io")(http);
var Game = require("./server/Game");
var game = new Game();

game.start(io);

io.on("connection", function(socket) {
  game.socketConnectionEvent(socket);
});
